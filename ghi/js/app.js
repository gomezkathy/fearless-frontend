function createCard(name, description, pictureUrl, starts, ends, locationName) {
  return `
    <div class="card">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <p class="card-text">${locationName}</p>
        <p class="card-text">${description}</p>
        <p class="card-footer">${starts}-${ends}</p>
      </div>
    </div>
  `;
}
  window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);

        if(!response.ok) {
            console.error('An error has occurred');
        } else {
            const data = await response.json();

            const columns = document.querySelectorAll('.col');
            let columnIndex = 0;

            for (let conference of data.conferences) {
              const detailUrl = `http://localhost:8000${conference.href}`;
              const detailResponse = await fetch(detailUrl);
              if (detailResponse.ok) {
                const details = await detailResponse.json();
                const title = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const start = new Date(details.conference.starts);
                const starts = start.toLocaleDateString(undefined,{month:'numeric', day:'numeric', year:'numeric'});
                const end = new Date(details.conference.ends);
                const ends = end.toLocaleDateString(undefined,{month:'numeric', day:'numeric', year:'numeric'});
                const locationName = details.conference.location.name;
                const html = createCard(title, description, pictureUrl,starts, ends, locationName);
                const column = columns[columnIndex];
                column.innerHTML += html;
                columnIndex = (columnIndex + 1) % columns.length
              }
            }

        }
    } catch (e) {
      // Figure out what to do if an error is raised
      console.error(e);
      const errorMessageElement = document.getElementById('error-message');
      errorMessageElement.textContent = 'An Error Occured';
      errorMessageElement.classList.remove('d-none');
    }
  });
